# -*- coding:utf-8  -*-
from django.shortcuts import HttpResponseRedirect

def index(request):

    return HttpResponseRedirect("/main/categories")
