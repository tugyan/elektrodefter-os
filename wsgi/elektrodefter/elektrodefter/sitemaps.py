from django.contrib.sitemaps import Sitemap

from entrysystem.models import Entry

class MySiteMap(Sitemap):
    changefreq = "weekly"
    priority = 0.5
    
    def items(self):
        return Entry.objects.filter(draft=False)
