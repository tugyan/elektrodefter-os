# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('entrysystem', '0006_auto_20150222_2057'),
    ]

    operations = [
        migrations.AddField(
            model_name='entry',
            name='content',
            field=models.TextField(default=''),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='entry',
            name='last_edited',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 28, 14, 21, 59, 558261), verbose_name=b'editedtime'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='entry',
            name='time_created',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 28, 14, 21, 59, 558230), verbose_name=b'createdtime'),
            preserve_default=True,
        ),
    ]
