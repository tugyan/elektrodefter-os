from django.conf.urls import url
from . import views

urlpatterns = [
    # Examples:
    # url(r'^$', 'elektrodefter.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^(?P<category_name>[a-z]+)/' , views.showentries, name='show_entries'),
    url(r'(?P<url_name>[\w]*)/' , views.showEntry, name='showEntry'),
    url(r'^results/', views.showSearchResults, name="showsearchresults"),

]
