# -*- coding=utf-8 -*-
from django.db import models
from django.utils import timezone
from readermode.models import Category

# Create your models here.

class Entry(models.Model):
    title = models.CharField(verbose_name = "title", max_length=260)
    time_created = models.DateTimeField(verbose_name="createdtime", default = timezone.now)
    last_edited = models.DateTimeField(verbose_name="editedtime", default = timezone.now)
    written_by = models.CharField(verbose_name = "writtenby", default="Bilal Taşdelen", max_length=50)
    category = models.ForeignKey(Category, verbose_name = "category")
    summary = models.TextField(verbose_name="summary", blank=True)
    url_name = models.CharField(verbose_name="urlname", max_length=260)
    content = models.TextField()
    draft = models.BooleanField(default=False)
    
    def __str__(self):
        return 'Entry: ' + self.title

    def get_absolute_url(self):
        return '/main/entries/' + self.url_name

class Tag(models.Model):
    name = models.CharField(verbose_name = "tag", max_length=50)
    entries = models.ManyToManyField(Entry)
    

