from django.conf.urls import include, url
from . import views

urlpatterns = [
    # Examples:
    # url(r'^$', 'elektrodefter.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^categories/', views.showCategories ,name='showcategories'),
    url(r'^entries/', include('entrysystem.urls')),
]
