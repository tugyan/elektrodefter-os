# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('readermode', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='image',
            field=models.FilePathField(default='arduino.png', path=b'/home/bilal/workspace/elektrodefter/public/static/images/categories'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='category',
            name='title',
            field=models.CharField(default='Arduino', max_length=20, verbose_name=b'title'),
            preserve_default=False,
        ),
    ]
