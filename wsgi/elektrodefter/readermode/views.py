from django.shortcuts import render
from readermode.models import Category
from .forms import SearchForm
from entrysystem.models import Entry
from django.template.context import RequestContext
# Create your views here.

def showCategories(request):

    form = SearchForm()
    categories = Category.objects.filter()

    return render(request, 'readermode/categories.html', {'categories':categories, 'form': form})


def showSearchResults(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = SearchForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            search_query = form.cleaned_data['search_query']
            results = Entry.objects.filter(title__icontains=search_query, draft=False) | \
                Entry.objects.filter(content__icontains=search_query, draft=False)
            return render(request,"results.html", {'results' : results, 'form' : form}, context_instance=RequestContext(request))
    
    else:
        form = SearchForm()
        
    #categories = Category.objects.filter()
    #results = Entry.objects.filter(draft=False)
    return render(request,"results.html", {'results' : results, 'form' : form}, context_instance=RequestContext(request))




